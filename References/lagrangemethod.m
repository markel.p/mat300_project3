%------------------------------------------------------------------------------
% Subject: MAT300
% Function name: gaussjordanmethod.m
% Students name: Kerman Munitxa, Julia Guma
% 2/5/2020
%
% Description: Compute and display a polynomial given some points and mesh
%------------------------------------------------------------------------------

function lagrangemethod(mesh_t, vec_x, vec_y, vec_z, outputnodes)
  
  %Start the timer
   timer = tic;
   
   %Get the number of x and y coordinates
   size_mat = columns(vec_x);
   z_size = columns(vec_z);
   %L = ones(size_mat, size_mat);
   %x = linspace(mesh_t(1), mesh_t(size_mat), outputnodes);
   
   %x = 100;
   
    Lx = 0;
    Ly = 0;
    Lz = 0;
   
   %Create the L
   for i=0:1:(size_mat-1)     
     p = 1;
     for j = 0:1:(size_mat-1)
       
       if(i != j)
       
        result(1) = 1;
        result(2) = -mesh_t(j + 1);
       
        result = result ./ (mesh_t(i+1) - mesh_t(j + 1));
        p = conv(p, result);
      
       endif
    
      
     endfor
     
    px = vec_x(i+1) * p;
    py = vec_y(i+1) * p;
    Lx = Lx + px; 
    Ly = Ly + py;
    
    if(z_size != 0)
      pz = vec_z(i+1) * p;
      Lz = Lz + pz;
     
     endif
   endfor
   
   
   t = linspace(mesh_t(1), mesh_t(size_mat), outputnodes);
   

   
   
    if(z_size == 0)
       x = polyval(Lx, t);
       y = polyval(Ly, t);
   
       %Display the given points
       for n=1:1:size_mat
        plot(vec_x(n), vec_y(n), 'ro');
        hold on;  
       endfor
  
      %Display the polynomial 
      plot(x, y, 'b');
    else
       x = polyval(Lx, t);
       y = polyval(Ly, t);
       z = polyval(Lz, t);
   
       %Display the given points
       for n=1:1:size_mat
        plot3(vec_x(n), vec_y(n), vec_z(n),'ro');
        hold on;  
       endfor
  
      %Display the polynomial 
      plot3(x, y, z, 'b');
    endif
   
   
   toc(timer);%Stop the timer and display it  
  
endfunction