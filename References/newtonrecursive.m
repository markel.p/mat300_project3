%------------------------------------------------------------------------------
% Subject: MAT300
% Function name: newtonrecursive.m
% Students name: Kerman Munitxa, Julia Guma
% 2/5/2020
%
% Description: Calls recursively this function in order to compute the divided 
% differences
%------------------------------------------------------------------------------

function result = newtonrecursive(mesh_t, mesh_v)
  
  %Ending condition. If the mesh has only one column, we computed the 
  %last element of the divided differences
  if(columns(mesh_v) == 1)
    result = mesh_v(1);
  else
  size = columns(mesh_t);
  
  %Compute the left element of the numerator
  left = newtonrecursive(mesh_t(2:size), mesh_v(2:size));
  
  %Compute the right element of the numerator
  right = newtonrecursive(mesh_t(1:size - 1), mesh_v(1:size - 1));
  
  %Compute the numerator
  result = left - right;
  
  %Divide the numerator by the nodes
  result = result / (mesh_t(size) - mesh_t(1));
  
  endif
end