%------------------------------------------------------------------------------
% Subject: MAT300
% Function name: newtonmethod.m
% Students name: Kerman Munitxa, Julia Guma
% 2/5/2020
%
% Description: Compute and display a polynomial given some points and mesh 
% using the Newton method
%------------------------------------------------------------------------------

function newtonmethod(mesh_t, vec_x, vec_y, vec_z, outputnodes)
  
  %Start the timer
   timer = tic;
   
   %Get the number of x and y coordinates
   size_mat = columns(vec_x);
      
   %Compute the divided differences for x and y (f[x0] and f[y0])
   px(size_mat) = newtonrecursive(mesh_t(1), vec_x(1));
   py(size_mat) = newtonrecursive(mesh_t(1), vec_y(1));
   
   %If it is in 3 dimensions, compute the divided differnces for z (f[z0])
   if(columns(vec_z))
      pz(size_mat) = newtonrecursive(mesh_t(1), vec_z(1));
   endif
   
   %Compute the Newton polynomials
   for i=2:1:(size_mat)
     
     n = 1;
     for j = 1:1:i-1
              
        %Set the first element as 1 and the rest is the mesh
        result(1) = 1;
        result(2) = -mesh_t(j);
        
        %Keep track of the previous Newton polynomials and create the 
        %new polynomial
        n = conv(n, result);       
        
     endfor
     
     %Check if we are in 3d
     if(columns(vec_z))
     
      %Compute the divided differences for it
      fz = newtonrecursive(mesh_t(1 : i), vec_z(1 : i));
      
      %Multply it by the previous polynomials
      fz = fz * n;
      
      %In order to add two polynomials of diferent degree, we have to add 0 to 
      %the smallest polynomial so we can add them
      new_fz = [zeros(1,max(0,numel(py) - numel(fz))), fz];
      
      %Add the polynomials
      pz = pz + new_fz;
     endif
     
     %Compute the divided differences for them
     fx = newtonrecursive(mesh_t(1 : i), vec_x(1 : i));
     fy = newtonrecursive(mesh_t(1 : i), vec_y(1 : i));
     
      %Multply them by the previous polynomials
     fx = fx * n;
     fy = fy * n;
    
     %Add the polynomials
     new_fx = [zeros(1,max(0,numel(px) - numel(fx))), fx];
     new_fy = [zeros(1,max(0,numel(py) - numel(fy))), fy];    
     px = px + new_fx;
     py = py + new_fy;
    
    
   endfor
   
   %Create mesh of t
   t = linspace(mesh_t(1), mesh_t(size_mat), outputnodes);
   
   %Check if we are in 3d
    if(columns(vec_z) == 0)
    
       %Get the set of values for the x and y of their polynomials given a mesh
       x = polyval(px, t);
       y = polyval(py, t);
   
       %Display the given points
       for n=1:1:size_mat
        plot(vec_x(n), vec_y(n), 'ro');
        hold on;  
       endfor
  
      %Display the polynomial 
      plot(x, y, 'b');
    else
      %Get the set of values for the x, y and z of their polynomials given a mesh
       x = polyval(px, t);
       y = polyval(py, t);
       z = polyval(pz, t);
   
       %Display the given points
       for n=1:1:size_mat
        plot3(vec_x(n), vec_y(n), vec_z(n),'ro');
        hold on;  
       endfor
  
      %Display the polynomial 
      plot3(x, y, z, 'b');
    endif
  
   %Display the polynomial 
   plot(x, y, 'b');
   
   toc(timer);%Stop the timer and display it  
  
  
endfunction