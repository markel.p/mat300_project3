%------------------------------------------------------------------------------
% Subject: MAT300
% Function name: midsubdivision.m
% Students name: Kerman Munitxa, Julia Guma, Markel Pisano
% 3/30/2020
%
% Description: Implement Midpoint subdivision algorithm
%------------------------------------------------------------------------------

function midsubdivision(data)
  
  
  %Get the data
  eval(data)
  
  %calculate number of points at final curve
  k = 2**miditerations + 1;
  
  % call the recursive function
  [x,y,z] = midpoint_recursive(1, miditerations, PX, PY, PZ, Dimension);
  %add extreme points
  x = [PX(1), x, PX(end)];
  y = [PY(1), y, PY(end)];
  z = [PZ(1), z, PZ(end)];
  % res vectors should be changed after recursive algorithm
  if length(x) != k
    error("Resultant vector should be of length %d, but is %d", k, length(x));
  endif


  % plot the resultant curve points
  if Dimension == 3
    %plot in 3D
    plot3(x,y,z, 'ro');
    hold on;
    plot3(x,y,z, 'r');
  else
    %plot in 2D
    plot(x,y, 'ro');
    hold on;
    plot(x,y, 'r');
  endif
  
  %plot control points (comment if original points not wanted)
  hold on;
  if Dimension == 3
    %plot in 3D
    plot3(PX,PY,PZ, 'g');
  else
    %plot in 2D
    plot(PX,PY, 'g');
  endif
  
  disp('midsubdivision');
endfunction

function [x,y,z] = midpoint_recursive(it, max_it, X, Y, Z, Dimension)
  %calcuate gamma(0.5) and get intermediate points for next iteration
  
  %t is always (1/2) -> thus MIDpoint subdivision...
  t = 0.5;
  
  %get number of control points
  n = length(X);
  %number of intermediate point on each iteration
  n_size = n-1;
  
  %keep track of previous column
  prev_x = X;
  prev_y = Y;
  prev_z = Z;
  
  %store intermediate points for next iteration
  curve_1_x = curve_1_y = curve_1_z = linspace(0, 0, n);
  curve_2_x = curve_2_y = curve_2_z = linspace(0, 0, n);
  %set first point, being the control point
  curve_1_x(1) = prev_x(1);
  curve_1_y(1) = prev_y(1);
  curve_1_z(1) = prev_z(1);
  curve_2_x(1) = prev_x(n);
  curve_2_y(1) = prev_y(n);
  curve_2_z(1) = prev_z(n);
  
  
  %foreach level of interpolation
  for i=2 : 1 : n
    %store current column (intermediate points)
    curr_x = curr_y = curr_z = linspace(0, 0, n_size);
    
    %foreach intermediate point at level i
    for j=1:1:n_size
      %compute current column interpolating previous column (n_size+1)
      curr_x(j)=t*prev_x(j+1)+t*prev_x(j);
      curr_y(j)=t*prev_y(j+1)+t*prev_y(j);
      
      %not caring about extra dimension overhead... or yes
      if Dimension == 3
        curr_z(j)=t*prev_z(j+1)+t*prev_z(j);
      endif
    endfor
    %update previous column
    prev_x = curr_x;
    prev_y = curr_y;
    prev_z = curr_z;
    
    %set control points for next iterations
    curve_1_x(i) = prev_x(1);
    curve_1_y(i) = prev_y(1);
    curve_1_z(i) = prev_z(1);
    %don't forget to invert this vectors
    curve_2_x(i) = prev_x(n_size);
    curve_2_y(i) = prev_y(n_size);
    curve_2_z(i) = prev_z(n_size);
    
    
    %update number of intermediate points at level i+1
    n_size=n_size-1;
  endfor
  
  curve_2_x = flip(curve_2_x);
  curve_2_y = flip(curve_2_y);
  curve_2_z = flip(curve_2_z);
  
  %sanity check, only one element (gamma)
  if length(prev_x) != 1
    error("Well, this should be length 1");
  endif
  
  %set gamma as new point
  x = prev_x(1);
  y = prev_y(1);
  z = prev_z(1);
    
  %end condition
  if it < max_it
    % call recursion
    [x1,y1,z1] = midpoint_recursive(it+1,max_it,curve_1_x, curve_1_y, curve_1_z, Dimension);
    [x2,y2,z2] = midpoint_recursive(it+1,max_it,curve_2_x, curve_2_y, curve_2_z, Dimension);
    
    %sandwich gamma between new curves
    x = [x1, x, x2];
    y = [y1, y, y2];
    z = [z1, z, z2];
     
  else
    if Dimension == 3
      plot3(X, Y, Z, 'b');
      hold on;
    else
      plot(X, Y, 'b');
      hold on;
    endif
  
  endif
endfunction
