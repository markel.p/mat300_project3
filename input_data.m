%------------------------------------------------------------------------------
% Subject: MAT300
% Function name: input_data.m
% Students name: Kerman Munitxa, Julia Guma, Markel Pisano
% 3/30/2020
%
% Description: Input data for testing the functions
%------------------------------------------------------------------------------

PX = [1 2 0];  % x-coordinate interpolation points
PY = [-1 3 4]; % y-coordinate interpolation points
PZ = [1 -1 2]; % z-coordinate interpolation points

Dimension = 2; %Dimension 2 or 3 (consistent with PZ)

methoddigit = 2; % 0 direct evaluation, 1 DeCasteljau, 2 midpoint subdivision

outputnodes = 400; % number of nodes for the output mesh

shellindex = 20; % node for which compute shells if methoddigit = 1

miditerations = 3; %number of iterations if methoddigit = 2