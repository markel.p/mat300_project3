%------------------------------------------------------------------------------
% Subject: MAT300
% Function name: decasteljau.m
% Students name: Kerman Munitxa, Julia Guma, Markel Pisano
% 3/21/2020
%
% Description: Compute the Bezier curve using decasteljau method
%------------------------------------------------------------------------------

function decasteljau(data)
  
  %Get the data
  eval(data)
  
  %Get the size of the mesh
  size = columns(PX);
   
  %Check if we are in 2 or three dimensions 
  PZ_data = [];  
  if(Dimension == 3)
    PZ_data = PZ;
  endif
  
  %Create the mesh
  t = linspace(0, 1, outputnodes + 1);
  
  %Get the number of times we should iterate until obtaining the final point
  init_size = size-1;
  n_size = init_size;
  
  %The first points are the ones given
  vec_x = PX;
  vec_y = PY;
  
   if(Dimension == 3)
      vec_z = PZ;
   endif
  
  %These will store the points on the curve
  result_x = linspace(0, 0, outputnodes);
  result_y = linspace(0, 0, outputnodes);
  
  if(Dimension == 3)
    result_z = linspace(0, 0, outputnodes);
   endif
  
  shell_plot_x = zeros(size-1);
  shell_plot_y = zeros(size-1);
  shell_plot_z = zeros(size-1);
  
  %Compute the points for every t
  for j=1:1:(outputnodes + 1)
  
    %Compute the shells for the corresponding t until getting one final point
    for i=1:1:(size-1)
    
      %Credate a vector of values for x, y and z
      A_x = linspace(0, 0, n_size);
      A_y = linspace(0, 0, n_size);
      A_z = linspace(0, 0, n_size);
    
      for n=1:1:n_size
        %Compute the new x, y and z
        A_x(n) = t(j) * vec_x(n+1) + (1 - t(j)) * vec_x(n);
        A_y(n) = t(j) * vec_y(n+1) + (1 - t(j)) * vec_y(n);
        
        if(Dimension == 3)
           A_z(n) = t(j) * vec_z(n+1) + (1 - t(j)) * vec_z(n);
        endif
      
      endfor
    
      %Pass the new obtained points to vec_x and vec_y so in the next iteration 
      %we take them as a base to construct the new points
      vec_x = A_x;
      vec_y = A_y;
      
      if(Dimension == 3)
        vec_z = A_z;
      endif
      
      %Store the corresponding shell
      if j == shellindex
        %In 3D
        if(Dimension == 3)
          for m=1:1:columns(A_x)
            shell_plot_x(i,m) = A_x(m);
            shell_plot_y(i,m) = A_y(m);
            shell_plot_z(i,m) = A_z(m);
          endfor   
       %In 2D   
        else
          for m=1:1:columns(A_x)
            shell_plot_x(i,m) = A_x(m);
            shell_plot_y(i,m) = A_y(m);
          endfor 
        endif
      endif
      
      %Decrease by 1 the size of the vector of points
      n_size = n_size - 1;
    endfor  
    
    %Store the resulting point of the curve
    result_x(j) = vec_x(1);
    result_y(j) = vec_y(1);
    
    if(Dimension == 3)
      result_z(j) = vec_z(1);
    endif
    
    %Restart the points to the initial ones
    vec_x = PX;
    vec_y = PY;
    
    if(Dimension == 3)
      vec_z = PZ;
    endif
    
    %Restart the number of times we iterate per t
    n_size = init_size;  
  endfor
   
  %Display the given points in 3D
  if(Dimension == 3)       
    %Display the control points
    plot3(PX, PY, PZ, 'bo');
    hold on;
    
    plot3(PX, PY, PZ, 'b');
    hold on;
    
    %Display the curve 
    plot3(result_x, result_y, result_z, 'r');
    hold on;
    
    %Display the shells
    for m=1:1:(size-1)
      s_x = shell_plot_x(m, 1:(size-1)-m+1);
      s_y = shell_plot_y(m, 1:(size-1)-m+1);
      s_z = shell_plot_z(m, 1:(size-1)-m+1);
      plot3(s_x, s_y, s_z,'b');  
     endfor

  %Display the given points in 2D
  else
    %Display the control points
    plot(PX, PY, 'bo');
    hold on;
    
    plot(PX, PY, 'b');
    hold on;
    
    %Display the curve 
    plot(result_x, result_y, 'r');
    hold on;
    
    %Display the shells
    for m=1:1:(size-1)
      s_x = shell_plot_x(m, 1:(size-1)-m+1);
      s_y = shell_plot_y(m, 1:(size-1)-m+1);
      plot(s_x, s_y,'b');  
     endfor
  endif

  
endfunction