The .zip contains:
  -beziercurve.m - Function that calls the corresponding method depending on the input data file.
  -decasteljau.m - Creates a Bezier curve using the De Casteljau algorithm. (Júlia)
  -directevaluation.m - Creates a Bezier curve using the direct evaluation algorithm. (Kerman)
  -midsubdivision.m - Creates a Bezier curve using the midpoint subdivision algorithm. (Markel)
  -input_data.m - Input file to test.
  -explanations.pdf - documentation about the algorithms.

We submitted an input file to test.

Instructions:
  -To run the code, you should call beziercurve.m passing the name of the input data file.
  -Example: beziercurve('input_data').

Input_data variables:
    -PX, PY, PZ - x, y and z axis of the points we input.
    -Dimension - dimension of the resulting curve, either 2 or 3
    -methoddigit - method wanted to use. 0 direct evaluation, 1 DeCasteljau, 2 midpoint subdivision.
    -outputnodes - number of nodes for the output mesh.
    -shellindex - nodes for compute shells in DeCasteljau.
    -miditerations - number of iterations in midpoint substitution.
  
  
For this assimgent we only used the slides, not any other reference
  
Authors:
  -Kerman Munitxa, Júlia Gumà, Markel Pisano.