%------------------------------------------------------------------------------
% Subject: MAT300
% Function name: beziercurve.m
% Students name: Kerman Munitxa, Julia Guma, Markel Pisano
% 3/21/2020
%
% Description: Call the different methods of bezier curves
%------------------------------------------------------------------------------

function beziercurve(data)
    
    eval(data);
    
  %Depending on the method, call the corresponding function
  if (methoddigit == 0)%direct
    directevaluation(data);    
  elseif(methoddigit == 1)%decasteljau
    decasteljau(data);   
  elseif(methoddigit == 2) %midpoint
    midsubdivision(data);
  endif 
  
endfunction