%------------------------------------------------------------------------------
% Subject: MAT300
% Function name: directevaluation.m
% Students name: Kerman Munitxa, Julia Guma, Markel Pisano
% 3/30/2020
%
% Description: Implement Bezier Curver by direct evaluation and Bernstein polynomials.
%------------------------------------------------------------------------------

function directevaluation(data)
  %Get the data
  eval(data)
  
  %Get the size of the mesh
  size = columns(PX);
   
  %Check if we are in 2 or three dimensions 
  PZ_data = [];  
  if(Dimension == 3)
    PZ_data = PZ;
  endif
  
  %constructing mesh [0,1]
  t = linspace(0, 1, outputnodes);
  
  %our results for x, y, and z
  result_x = zeros(1, outputnodes);
  result_y = zeros(1, outputnodes);
  if(Dimension == 3)
    result_z = zeros(1, outputnodes);
  endif
  
  
  n = size-1;
  
  %Loop over all the control points
  for i=0:1:n
  
    %calculating the bernstein polynomial of corresponding control point
    B = zeros(1,outputnodes);
    
    %Combinatory of n and i for the bernstein polynomial
    c = nchoosek(n,i);
    
    B = ((1 - t) .^(n-i)) .* (t .^ i) .* c;
  
    %adding the current polynomial multiplied by the control point to the result
    result_x += PX(i+1) .* B;
    result_y += PY(i+1) .* B;
    if(Dimension == 3)
      result_z += PZ(i+1) .* B;
    endif
    
  endfor

  %Display the given points in 3D
  if(Dimension == 3)       
    %Display the control points
    plot3(PX, PY, PZ, 'bo');
    hold on;
    
    plot3(PX, PY, PZ, 'b');
    hold on;
    
    %Display the curve 
    plot3(result_x, result_y, result_z, 'r');
    hold on;
    

  %Display the given points in 2D
  else
    %Display the control points
    plot(PX, PY, 'bo');
    hold on;
    
    plot(PX, PY, 'b');
    hold on;
    
    %Display the curve 
    plot(result_x, result_y, 'r');
    hold on;
    
  endif

  
endfunction